/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>

#include "test_dm_adapter.h"

int main(void) {

    int ret = 0;

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_dmadapter_load),
        cmocka_unit_test(test_dmadapter_connection),
        cmocka_unit_test(test_dmadapter_ManagementServer_GetParameterValue),
        cmocka_unit_test(test_dmadapter_ManagementServer_SetParameterValue),
        cmocka_unit_test(test_dmadapter_GetParameterNames_Parameter),
        cmocka_unit_test(test_dmadapter_GetParameterNames_EmptyPath_NextLevel_True),
        cmocka_unit_test(test_dmadapter_GetParameterNames_Object_NextLevel_True),
        cmocka_unit_test(test_dmadapter_GetParameterNames_Object_NextLevel_False),
        cmocka_unit_test(test_dmadapter_GetParameterNames_Object_NextLevel_False_Sorted),
        cmocka_unit_test(test_dmadapter_GetParameterNames_Object_NextLevel_False_searchPath),
        cmocka_unit_test(test_dmadapter_GetParameterNames_Object_NextLevel_True_searchPath),
        cmocka_unit_test(test_dmadapter_GetParameterNames_Parameter_searchPath),
        cmocka_unit_test(test_dmadapter_GetParameterNames_EmptyPath_NextLevel_false),
        cmocka_unit_test(test_dmadapter_GetParameterNames_DeviceObject_NextLevel_False),
        cmocka_unit_test(test_dmadapter_GetParameterNames_DeviceObject_NextLevel_True),
        cmocka_unit_test(test_dmadapter_GetParametersValues_Parameters),
        cmocka_unit_test(test_dmadapter_GetParametersValues_Parameter_noAccessRights),
        cmocka_unit_test(test_dmadapter_GetParametersValues_RootParameters),
        cmocka_unit_test(test_dmadapter_GetParametersValues_Object),
        cmocka_unit_test(test_dmadapter_gpv_001),
        cmocka_unit_test(test_dmadapter_Set),
        cmocka_unit_test(test_dmadapter_GetParametersValues_Object_Sorted),
        cmocka_unit_test(test_dmadapter_GetParametersValues_searchPath_Parameter),
        cmocka_unit_test(test_dmadapter_GetParametersValues_searchPath_Object),
        cmocka_unit_test(test_dmadapter_SetParameterValues),
        cmocka_unit_test(test_dmadapter_SetParameterValues_Faults),
        cmocka_unit_test(test_dmadapter_AddDeleteObject),
        cmocka_unit_test(test_dmadapter_test_0001_add_object_with_alias),
        cmocka_unit_test(test_dmadapter_SetParameterAttributes),
        cmocka_unit_test(test_dmadapter_GetParameterAttributes),
        cmocka_unit_test(test_dmadapter_Open_Close_Session),
        cmocka_unit_test(test_dmadapter_Reboot),
        cmocka_unit_test(test_dmadapter_FactoryReset)
    };
    ret = cmocka_run_group_tests(tests, test_dmadapter_setup, test_dmadapter_teardown);

    return ret;
}
